package service;

import io.grpc.stub.StreamObserver;
import proto.Person;

public class getInfos {
    public void getInfos(Person.Message request, StreamObserver<Person.Message> responseObserver) {
        System.out.println("test");

        Person.Message reply = Person.Message.newBuilder().setMsg("Hello").build();
        /* We can call multiple times onNext function if we have multiple replies, ex. in next commits */
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
        System.out.println("test");
    }


}
