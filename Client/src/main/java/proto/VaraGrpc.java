package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */

public final class VaraGrpc {

  private VaraGrpc() {}

  public static final String SERVICE_NAME = "Vara";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.Person.PersonDetalis,
      com.google.protobuf.Empty> getSetZodieVMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "setZodieV",
      requestType = proto.Person.PersonDetalis.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.Person.PersonDetalis,
      com.google.protobuf.Empty> getSetZodieVMethod() {
    io.grpc.MethodDescriptor<proto.Person.PersonDetalis, com.google.protobuf.Empty> getSetZodieVMethod;
    if ((getSetZodieVMethod = VaraGrpc.getSetZodieVMethod) == null) {
      synchronized (VaraGrpc.class) {
        if ((getSetZodieVMethod = VaraGrpc.getSetZodieVMethod) == null) {
          VaraGrpc.getSetZodieVMethod = getSetZodieVMethod = 
              io.grpc.MethodDescriptor.<proto.Person.PersonDetalis, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Vara", "setZodieV"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.Person.PersonDetalis.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new VaraMethodDescriptorSupplier("setZodieV"))
                  .build();
          }
        }
     }
     return getSetZodieVMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static VaraStub newStub(io.grpc.Channel channel) {
    return new VaraStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static VaraBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new VaraBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static VaraFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new VaraFutureStub(channel);
  }

  /**
   */
  public static abstract class VaraImplBase implements io.grpc.BindableService {

    /**
     */
    public void setZodieV(proto.Person.PersonDetalis request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getSetZodieVMethod(), responseObserver);

    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSetZodieVMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.Person.PersonDetalis,
                com.google.protobuf.Empty>(
                  this, METHODID_SET_ZODIE_V)))
          .build();
    }
  }

  /**
   */
  public static final class VaraStub extends io.grpc.stub.AbstractStub<VaraStub> {
    private VaraStub(io.grpc.Channel channel) {
      super(channel);
    }

    private VaraStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected VaraStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new VaraStub(channel, callOptions);
    }

    /**
     */
    public void setZodieV(proto.Person.PersonDetalis request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetZodieVMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class VaraBlockingStub extends io.grpc.stub.AbstractStub<VaraBlockingStub> {
    private VaraBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private VaraBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected VaraBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new VaraBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.google.protobuf.Empty setZodieV(proto.Person.PersonDetalis request) {
      return blockingUnaryCall(
          getChannel(), getSetZodieVMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class VaraFutureStub extends io.grpc.stub.AbstractStub<VaraFutureStub> {
    private VaraFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private VaraFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected VaraFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new VaraFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> setZodieV(
        proto.Person.PersonDetalis request) {
      return futureUnaryCall(
          getChannel().newCall(getSetZodieVMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SET_ZODIE_V = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final VaraImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(VaraImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SET_ZODIE_V:
          serviceImpl.setZodieV((proto.Person.PersonDetalis) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class VaraBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    VaraBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.Person.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Vara");
    }
  }

  private static final class VaraFileDescriptorSupplier
      extends VaraBaseDescriptorSupplier {
    VaraFileDescriptorSupplier() {}
  }

  private static final class VaraMethodDescriptorSupplier
      extends VaraBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    VaraMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (VaraGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new VaraFileDescriptorSupplier())
              .addMethod(getSetZodieVMethod())
              .build();
        }
      }
    }
    return result;
  }
}
