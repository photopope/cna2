package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: person.proto")
public final class PrimavaeraGrpc {

  private PrimavaeraGrpc() {}

  public static final String SERVICE_NAME = "Primavaera";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.Person.PersonDetalis,
      com.google.protobuf.Empty> getSetZodiePMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "setZodieP",
      requestType = proto.Person.PersonDetalis.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.Person.PersonDetalis,
      com.google.protobuf.Empty> getSetZodiePMethod() {
    io.grpc.MethodDescriptor<proto.Person.PersonDetalis, com.google.protobuf.Empty> getSetZodiePMethod;
    if ((getSetZodiePMethod = PrimavaeraGrpc.getSetZodiePMethod) == null) {
      synchronized (PrimavaeraGrpc.class) {
        if ((getSetZodiePMethod = PrimavaeraGrpc.getSetZodiePMethod) == null) {
          PrimavaeraGrpc.getSetZodiePMethod = getSetZodiePMethod = 
              io.grpc.MethodDescriptor.<proto.Person.PersonDetalis, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Primavaera", "setZodieP"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.Person.PersonDetalis.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new PrimavaeraMethodDescriptorSupplier("setZodieP"))
                  .build();
          }
        }
     }
     return getSetZodiePMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static PrimavaeraStub newStub(io.grpc.Channel channel) {
    return new PrimavaeraStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static PrimavaeraBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new PrimavaeraBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static PrimavaeraFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new PrimavaeraFutureStub(channel);
  }

  /**
   */
  public static abstract class PrimavaeraImplBase implements io.grpc.BindableService {

    /**
     */
    public void setZodieP(proto.Person.PersonDetalis request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getSetZodiePMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSetZodiePMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.Person.PersonDetalis,
                com.google.protobuf.Empty>(
                  this, METHODID_SET_ZODIE_P)))
          .build();
    }
  }

  /**
   */
  public static final class PrimavaeraStub extends io.grpc.stub.AbstractStub<PrimavaeraStub> {
    private PrimavaeraStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PrimavaeraStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PrimavaeraStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PrimavaeraStub(channel, callOptions);
    }

    /**
     */
    public void setZodieP(proto.Person.PersonDetalis request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetZodiePMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class PrimavaeraBlockingStub extends io.grpc.stub.AbstractStub<PrimavaeraBlockingStub> {
    private PrimavaeraBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PrimavaeraBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PrimavaeraBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PrimavaeraBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.google.protobuf.Empty setZodieP(proto.Person.PersonDetalis request) {
      return blockingUnaryCall(
          getChannel(), getSetZodiePMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class PrimavaeraFutureStub extends io.grpc.stub.AbstractStub<PrimavaeraFutureStub> {
    private PrimavaeraFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PrimavaeraFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PrimavaeraFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PrimavaeraFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> setZodieP(
        proto.Person.PersonDetalis request) {
      return futureUnaryCall(
          getChannel().newCall(getSetZodiePMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SET_ZODIE_P = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final PrimavaeraImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(PrimavaeraImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SET_ZODIE_P:
          serviceImpl.setZodieP((proto.Person.PersonDetalis) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class PrimavaeraBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    PrimavaeraBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.Person.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Primavaera");
    }
  }

  private static final class PrimavaeraFileDescriptorSupplier
      extends PrimavaeraBaseDescriptorSupplier {
    PrimavaeraFileDescriptorSupplier() {}
  }

  private static final class PrimavaeraMethodDescriptorSupplier
      extends PrimavaeraBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    PrimavaeraMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (PrimavaeraGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new PrimavaeraFileDescriptorSupplier())
              .addMethod(getSetZodiePMethod())
              .build();
        }
      }
    }
    return result;
  }
}
