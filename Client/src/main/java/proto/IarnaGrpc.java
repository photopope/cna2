package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: person.proto")
public final class IarnaGrpc {

  private IarnaGrpc() {}

  public static final String SERVICE_NAME = "Iarna";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.Person.PersonDetalis,
      com.google.protobuf.Empty> getSetZodieIMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "setZodieI",
      requestType = proto.Person.PersonDetalis.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.Person.PersonDetalis,
      com.google.protobuf.Empty> getSetZodieIMethod() {
    io.grpc.MethodDescriptor<proto.Person.PersonDetalis, com.google.protobuf.Empty> getSetZodieIMethod;
    if ((getSetZodieIMethod = IarnaGrpc.getSetZodieIMethod) == null) {
      synchronized (IarnaGrpc.class) {
        if ((getSetZodieIMethod = IarnaGrpc.getSetZodieIMethod) == null) {
          IarnaGrpc.getSetZodieIMethod = getSetZodieIMethod = 
              io.grpc.MethodDescriptor.<proto.Person.PersonDetalis, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Iarna", "setZodieI"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.Person.PersonDetalis.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new IarnaMethodDescriptorSupplier("setZodieI"))
                  .build();
          }
        }
     }
     return getSetZodieIMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static IarnaStub newStub(io.grpc.Channel channel) {
    return new IarnaStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static IarnaBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new IarnaBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static IarnaFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new IarnaFutureStub(channel);
  }

  /**
   */
  public static abstract class IarnaImplBase implements io.grpc.BindableService {

    /**
     */
    public void setZodieI(proto.Person.PersonDetalis request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getSetZodieIMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSetZodieIMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.Person.PersonDetalis,
                com.google.protobuf.Empty>(
                  this, METHODID_SET_ZODIE_I)))
          .build();
    }
  }

  /**
   */
  public static final class IarnaStub extends io.grpc.stub.AbstractStub<IarnaStub> {
    private IarnaStub(io.grpc.Channel channel) {
      super(channel);
    }

    private IarnaStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected IarnaStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new IarnaStub(channel, callOptions);
    }

    /**
     */
    public void setZodieI(proto.Person.PersonDetalis request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetZodieIMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class IarnaBlockingStub extends io.grpc.stub.AbstractStub<IarnaBlockingStub> {
    private IarnaBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private IarnaBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected IarnaBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new IarnaBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.google.protobuf.Empty setZodieI(proto.Person.PersonDetalis request) {
      return blockingUnaryCall(
          getChannel(), getSetZodieIMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class IarnaFutureStub extends io.grpc.stub.AbstractStub<IarnaFutureStub> {
    private IarnaFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private IarnaFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected IarnaFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new IarnaFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> setZodieI(
        proto.Person.PersonDetalis request) {
      return futureUnaryCall(
          getChannel().newCall(getSetZodieIMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SET_ZODIE_I = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final IarnaImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(IarnaImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SET_ZODIE_I:
          serviceImpl.setZodieI((proto.Person.PersonDetalis) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class IarnaBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    IarnaBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.Person.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Iarna");
    }
  }

  private static final class IarnaFileDescriptorSupplier
      extends IarnaBaseDescriptorSupplier {
    IarnaFileDescriptorSupplier() {}
  }

  private static final class IarnaMethodDescriptorSupplier
      extends IarnaBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    IarnaMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (IarnaGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new IarnaFileDescriptorSupplier())
              .addMethod(getSetZodieIMethod())
              .build();
        }
      }
    }
    return result;
  }
}
