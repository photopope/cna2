package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: person.proto")
public final class ToamnaGrpc {

  private ToamnaGrpc() {}

  public static final String SERVICE_NAME = "Toamna";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.Person.PersonDetalis,
      com.google.protobuf.Empty> getSetZodieTMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "setZodieT",
      requestType = proto.Person.PersonDetalis.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.Person.PersonDetalis,
      com.google.protobuf.Empty> getSetZodieTMethod() {
    io.grpc.MethodDescriptor<proto.Person.PersonDetalis, com.google.protobuf.Empty> getSetZodieTMethod;
    if ((getSetZodieTMethod = ToamnaGrpc.getSetZodieTMethod) == null) {
      synchronized (ToamnaGrpc.class) {
        if ((getSetZodieTMethod = ToamnaGrpc.getSetZodieTMethod) == null) {
          ToamnaGrpc.getSetZodieTMethod = getSetZodieTMethod = 
              io.grpc.MethodDescriptor.<proto.Person.PersonDetalis, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Toamna", "setZodieT"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.Person.PersonDetalis.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new ToamnaMethodDescriptorSupplier("setZodieT"))
                  .build();
          }
        }
     }
     return getSetZodieTMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ToamnaStub newStub(io.grpc.Channel channel) {
    return new ToamnaStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ToamnaBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ToamnaBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ToamnaFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ToamnaFutureStub(channel);
  }

  /**
   */
  public static abstract class ToamnaImplBase implements io.grpc.BindableService {

    /**
     */
    public void setZodieT(proto.Person.PersonDetalis request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getSetZodieTMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSetZodieTMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.Person.PersonDetalis,
                com.google.protobuf.Empty>(
                  this, METHODID_SET_ZODIE_T)))
          .build();
    }
  }

  /**
   */
  public static final class ToamnaStub extends io.grpc.stub.AbstractStub<ToamnaStub> {
    private ToamnaStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ToamnaStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ToamnaStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ToamnaStub(channel, callOptions);
    }

    /**
     */
    public void setZodieT(proto.Person.PersonDetalis request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetZodieTMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ToamnaBlockingStub extends io.grpc.stub.AbstractStub<ToamnaBlockingStub> {
    private ToamnaBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ToamnaBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ToamnaBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ToamnaBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.google.protobuf.Empty setZodieT(proto.Person.PersonDetalis request) {
      return blockingUnaryCall(
          getChannel(), getSetZodieTMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ToamnaFutureStub extends io.grpc.stub.AbstractStub<ToamnaFutureStub> {
    private ToamnaFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ToamnaFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ToamnaFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ToamnaFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> setZodieT(
        proto.Person.PersonDetalis request) {
      return futureUnaryCall(
          getChannel().newCall(getSetZodieTMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SET_ZODIE_T = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ToamnaImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ToamnaImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SET_ZODIE_T:
          serviceImpl.setZodieT((proto.Person.PersonDetalis) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ToamnaBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ToamnaBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.Person.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Toamna");
    }
  }

  private static final class ToamnaFileDescriptorSupplier
      extends ToamnaBaseDescriptorSupplier {
    ToamnaFileDescriptorSupplier() {}
  }

  private static final class ToamnaMethodDescriptorSupplier
      extends ToamnaBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ToamnaMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ToamnaGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ToamnaFileDescriptorSupplier())
              .addMethod(getSetZodieTMethod())
              .build();
        }
      }
    }
    return result;
  }
}
