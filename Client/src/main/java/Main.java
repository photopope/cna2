import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import proto.MessengerGrpc;
import proto.Person;
import proto.PersonServiceGrpc;




import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();

        PersonServiceGrpc.PersonServiceBlockingStub personStub = PersonServiceGrpc.newBlockingStub(channel);
        MessengerGrpc.MessengerBlockingStub mesaj=MessengerGrpc.newBlockingStub(channel);

      DataValidator valid=new DataValidator("MM/dd/yyyy");

        boolean isRunning = true;
        while(isRunning)
        {


            Scanner myObj = new Scanner(System.in);  // Create a Scanner object

            System.out.print("Anul nasteri: ");
            String an = myObj.nextLine();  // Read user input
            System.out.print("Ziua nasteri: ");
            String zi = myObj.nextLine();  // Read user input
            System.out.print("Luna nasteri: ");
            String luna = myObj.nextLine();  // Read user input
            String data=luna+"/"+zi+"/"+an;
            System.out.println(data);
            if(!valid.isValid(data))
            {
                while(!valid.isValid(data))
                {
                    System.out.println("Data invalida, scrieti din nou! ");
                    System.out.print("Anul nasteri: ");
                     an = myObj.nextLine();  // Read user input
                    System.out.print("Ziua nasteri: ");
                   zi = myObj.nextLine();  // Read user input
                    System.out.print("Luna nasteri: ");
                    luna = myObj.nextLine();  // Read user input
                   data = luna + "/" + zi + "/" + an;
                    System.out.println(data);
                }


            }
            //bookStub.sendDate("date");

            personStub.sendDate(Person.PersonDetalis.newBuilder().setZi(zi).setLuna(luna).setAn(an).build());
            System.out.println("testttt");

            Person.Message reply = personStub.getInfos(Person.Message.newBuilder().build());
            System.out.println("testttt");

            System.out.println(reply.getMsg());

        }
        channel.shutdown();

    }
}
